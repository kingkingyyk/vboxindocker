# Running VirtualBox in Docker

Current release is for Ubuntu 20.04
Steps :
- Clone / Copy the `script.sh` to your local machine.
- Run `chmod 777 script.sh`
- Run `sudo docker run --rm -it --privileged=true --network=host -v=<path to script.sh>:/script.sh -v=<path to os images>:/images ubuntu:focal /bin/bash`
- Once you are in the container, run `sh /script.sh` and wait for the commands to complete
- Open `http://localhost/phpvirtualbox` to login to virtualbox. The default credential is admin/admin, which is given by phpvirtualbox
- If you want to view the remote desktop server from different host, run `VBoxManage modifyvm <VM NAME> --vrdeaddress 0.0.0.0` after creating the VM.
