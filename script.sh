export TZ=Asia/Kuala_Lumpur
echo $TZ > /etc/timezone
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
apt update
apt upgrade -y
apt dist-upgrade -y
apt-get install -y dkms software-properties-common wget gnupg gnupg2 gnupg1 procps linux-headers-$(uname -r) libsdl-ttf2.0-0 kmod linux-image-$(uname -r) gcc make build-essential dpkg-dev binutils unzip
wget http://archive.ubuntu.com/ubuntu/pool/main/libv/libvpx/libvpx5_1.7.0-3_amd64.deb
dpkg -i libvpx5_1.7.0-3_amd64.deb
rm libvpx5_1.7.0-3_amd64.deb

wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian eoan contrib"
apt-get install -y virtualbox-6.1
LatestVirtualBoxVersion=$(wget -qO - https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT) && wget "https://download.virtualbox.org/virtualbox/${LatestVirtualBoxVersion}/Oracle_VM_VirtualBox_Extension_Pack-${LatestVirtualBoxVersion}.vbox-extpack"
echo "y" | VBoxManage extpack install --replace Oracle_VM_VirtualBox_Extension_Pack-${LatestVirtualBoxVersion}.vbox-extpack
rm Oracle_VM_VirtualBox_Extension_Pack-${LatestVirtualBoxVersion}.vbox-extpack

wget https://github.com/phpvirtualbox/phpvirtualbox/archive/develop.zip -O phpvbox.zip
unzip phpvbox.zip
rm phpvbox.zip
mv phpvirtualbox-develop /usr/share/phpvirtualbox
cd /usr/share/phpvirtualbox
cp config.php-example config.php
apt-get install -y apache2 libapache2-mod-php php php-soap php-xml
a2dismod mpm_event mpm_worker
a2enmod mpm_prefork php7.4
sed -i '/Require local/d' phpvirtualbox.conf
cp phpvirtualbox.conf /etc/apache2/sites-enabled/
service apache2 restart

useradd -p $(openssl passwd -1 pass) vbox

echo VBOXWEB_USER=vbox > /etc/default/virtualbox

apt clean
apt autoclean
rm -rf /var/lib/apt/lists/*

vboxwebsrv